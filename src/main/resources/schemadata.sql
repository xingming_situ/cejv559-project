-- MySQL dump 10.13  Distrib 8.0.21, for Win64 (x86_64)
--
-- Host: localhost    Database: test1
-- ------------------------------------------------------
-- Server version	8.0.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `course`
--

DROP TABLE IF EXISTS `course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `course` (
  `course_id` int NOT NULL AUTO_INCREMENT,
  `course_code` varchar(255) NOT NULL,
  `course_cost` double NOT NULL,
  `description` varchar(255) NOT NULL,
  `disabled` bit(1) DEFAULT NULL,
  `teacher_id` int DEFAULT NULL,
  PRIMARY KEY (`course_id`),
  KEY `FKsybhlxoejr4j3teomm5u2bx1n` (`teacher_id`),
  CONSTRAINT `FKsybhlxoejr4j3teomm5u2bx1n` FOREIGN KEY (`teacher_id`) REFERENCES `teacher` (`teacher_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course`
--

LOCK TABLES `course` WRITE;
/*!40000 ALTER TABLE `course` DISABLE KEYS */;
INSERT INTO `course` VALUES (1,'code:1',100.99,'description:1',_binary '\0',2),(2,'code:2',100.99,'description:2',_binary '\0',3),(3,'code:3',100.99,'description:3',_binary '\0',4),(4,'code:4',100.99,'description:4',_binary '\0',5),(5,'code:5',100.99,'description:5',_binary '\0',6),(6,'code:6',100.99,'description:6',_binary '\0',7),(7,'code:7',100.99,'description:7',_binary '\0',8),(8,'code:8',100.99,'description:8',_binary '\0',9),(9,'code:9',100.99,'description:9',_binary '\0',10),(10,'code:10',100.99,'description:10',_binary '\0',1);
/*!40000 ALTER TABLE `course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `department`
--

DROP TABLE IF EXISTS `department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `department` (
  `department_id` int NOT NULL AUTO_INCREMENT,
  `department_code` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `disabled` bit(1) DEFAULT NULL,
  PRIMARY KEY (`department_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `department`
--

LOCK TABLES `department` WRITE;
/*!40000 ALTER TABLE `department` DISABLE KEYS */;
INSERT INTO `department` VALUES (1,'department:1','description:1',_binary '\0'),(2,'department:2','description:2',_binary '\0'),(3,'department:3','description:3',_binary '\0'),(4,'department:4','description:4',_binary '\0'),(5,'department:5','description:5',_binary '\0'),(6,'department:6','description:6',_binary '\0'),(7,'department:7','description:7',_binary '\0'),(8,'department:8','description:8',_binary '\0'),(9,'department:9','description:9',_binary '\0'),(10,'department:10','description:10',_binary '\0');
/*!40000 ALTER TABLE `department` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registration`
--

DROP TABLE IF EXISTS `registration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `registration` (
  `registration_id` int NOT NULL AUTO_INCREMENT,
  `registration_code` varchar(255) NOT NULL,
  `course_id` int DEFAULT NULL,
  `student_id` int DEFAULT NULL,
  `term_id` int DEFAULT NULL,
  PRIMARY KEY (`registration_id`),
  KEY `FKo0ifq05dge7ssg2voqs6wtik6` (`course_id`),
  KEY `FKcxvegulu1x4mjcvy3116tu5xu` (`student_id`),
  KEY `FK18ji4mxyw5738n5d4s2qlhwn4` (`term_id`),
  CONSTRAINT `FK18ji4mxyw5738n5d4s2qlhwn4` FOREIGN KEY (`term_id`) REFERENCES `term` (`term_id`),
  CONSTRAINT `FKcxvegulu1x4mjcvy3116tu5xu` FOREIGN KEY (`student_id`) REFERENCES `student` (`student_id`),
  CONSTRAINT `FKo0ifq05dge7ssg2voqs6wtik6` FOREIGN KEY (`course_id`) REFERENCES `course` (`course_id`)
) ENGINE=InnoDB AUTO_INCREMENT=201 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registration`
--

LOCK TABLES `registration` WRITE;
/*!40000 ALTER TABLE `registration` DISABLE KEYS */;
INSERT INTO `registration` VALUES (1,'code:1',2,2,2),(2,'code:2',3,3,1),(3,'code:3',4,4,2),(4,'code:4',5,5,1),(5,'code:5',6,6,2),(6,'code:6',7,7,1),(7,'code:7',8,8,2),(8,'code:8',9,9,1),(9,'code:9',10,10,2),(10,'code:10',1,11,1),(11,'code:11',2,12,2),(12,'code:12',3,13,1),(13,'code:13',4,14,2),(14,'code:14',5,15,1),(15,'code:15',6,16,2),(16,'code:16',7,17,1),(17,'code:17',8,18,2),(18,'code:18',9,19,1),(19,'code:19',10,20,2),(20,'code:20',1,21,1),(21,'code:21',2,22,2),(22,'code:22',3,23,1),(23,'code:23',4,24,2),(24,'code:24',5,25,1),(25,'code:25',6,26,2),(26,'code:26',7,27,1),(27,'code:27',8,28,2),(28,'code:28',9,29,1),(29,'code:29',10,30,2),(30,'code:30',1,31,1),(31,'code:31',2,32,2),(32,'code:32',3,33,1),(33,'code:33',4,34,2),(34,'code:34',5,35,1),(35,'code:35',6,36,2),(36,'code:36',7,37,1),(37,'code:37',8,38,2),(38,'code:38',9,39,1),(39,'code:39',10,40,2),(40,'code:40',1,41,1),(41,'code:41',2,42,2),(42,'code:42',3,43,1),(43,'code:43',4,44,2),(44,'code:44',5,45,1),(45,'code:45',6,46,2),(46,'code:46',7,47,1),(47,'code:47',8,48,2),(48,'code:48',9,49,1),(49,'code:49',10,50,2),(50,'code:50',1,51,1),(51,'code:51',2,52,2),(52,'code:52',3,53,1),(53,'code:53',4,54,2),(54,'code:54',5,55,1),(55,'code:55',6,56,2),(56,'code:56',7,57,1),(57,'code:57',8,58,2),(58,'code:58',9,59,1),(59,'code:59',10,60,2),(60,'code:60',1,61,1),(61,'code:61',2,62,2),(62,'code:62',3,63,1),(63,'code:63',4,64,2),(64,'code:64',5,65,1),(65,'code:65',6,66,2),(66,'code:66',7,67,1),(67,'code:67',8,68,2),(68,'code:68',9,69,1),(69,'code:69',10,70,2),(70,'code:70',1,71,1),(71,'code:71',2,72,2),(72,'code:72',3,73,1),(73,'code:73',4,74,2),(74,'code:74',5,75,1),(75,'code:75',6,76,2),(76,'code:76',7,77,1),(77,'code:77',8,78,2),(78,'code:78',9,79,1),(79,'code:79',10,80,2),(80,'code:80',1,81,1),(81,'code:81',2,82,2),(82,'code:82',3,83,1),(83,'code:83',4,84,2),(84,'code:84',5,85,1),(85,'code:85',6,86,2),(86,'code:86',7,87,1),(87,'code:87',8,88,2),(88,'code:88',9,89,1),(89,'code:89',10,90,2),(90,'code:90',1,91,1),(91,'code:91',2,92,2),(92,'code:92',3,93,1),(93,'code:93',4,94,2),(94,'code:94',5,95,1),(95,'code:95',6,96,2),(96,'code:96',7,97,1),(97,'code:97',8,98,2),(98,'code:98',9,99,1),(99,'code:99',10,100,2),(100,'code:100',1,1,1),(101,'code:101',2,2,2),(102,'code:102',3,3,1),(103,'code:103',4,4,2),(104,'code:104',5,5,1),(105,'code:105',6,6,2),(106,'code:106',7,7,1),(107,'code:107',8,8,2),(108,'code:108',9,9,1),(109,'code:109',10,10,2),(110,'code:110',1,11,1),(111,'code:111',2,12,2),(112,'code:112',3,13,1),(113,'code:113',4,14,2),(114,'code:114',5,15,1),(115,'code:115',6,16,2),(116,'code:116',7,17,1),(117,'code:117',8,18,2),(118,'code:118',9,19,1),(119,'code:119',10,20,2),(120,'code:120',1,21,1),(121,'code:121',2,22,2),(122,'code:122',3,23,1),(123,'code:123',4,24,2),(124,'code:124',5,25,1),(125,'code:125',6,26,2),(126,'code:126',7,27,1),(127,'code:127',8,28,2),(128,'code:128',9,29,1),(129,'code:129',10,30,2),(130,'code:130',1,31,1),(131,'code:131',2,32,2),(132,'code:132',3,33,1),(133,'code:133',4,34,2),(134,'code:134',5,35,1),(135,'code:135',6,36,2),(136,'code:136',7,37,1),(137,'code:137',8,38,2),(138,'code:138',9,39,1),(139,'code:139',10,40,2),(140,'code:140',1,41,1),(141,'code:141',2,42,2),(142,'code:142',3,43,1),(143,'code:143',4,44,2),(144,'code:144',5,45,1),(145,'code:145',6,46,2),(146,'code:146',7,47,1),(147,'code:147',8,48,2),(148,'code:148',9,49,1),(149,'code:149',10,50,2),(150,'code:150',1,51,1),(151,'code:151',2,52,2),(152,'code:152',3,53,1),(153,'code:153',4,54,2),(154,'code:154',5,55,1),(155,'code:155',6,56,2),(156,'code:156',7,57,1),(157,'code:157',8,58,2),(158,'code:158',9,59,1),(159,'code:159',10,60,2),(160,'code:160',1,61,1),(161,'code:161',2,62,2),(162,'code:162',3,63,1),(163,'code:163',4,64,2),(164,'code:164',5,65,1),(165,'code:165',6,66,2),(166,'code:166',7,67,1),(167,'code:167',8,68,2),(168,'code:168',9,69,1),(169,'code:169',10,70,2),(170,'code:170',1,71,1),(171,'code:171',2,72,2),(172,'code:172',3,73,1),(173,'code:173',4,74,2),(174,'code:174',5,75,1),(175,'code:175',6,76,2),(176,'code:176',7,77,1),(177,'code:177',8,78,2),(178,'code:178',9,79,1),(179,'code:179',10,80,2),(180,'code:180',1,81,1),(181,'code:181',2,82,2),(182,'code:182',3,83,1),(183,'code:183',4,84,2),(184,'code:184',5,85,1),(185,'code:185',6,86,2),(186,'code:186',7,87,1),(187,'code:187',8,88,2),(188,'code:188',9,89,1),(189,'code:189',10,90,2),(190,'code:190',1,91,1),(191,'code:191',2,92,2),(192,'code:192',3,93,1),(193,'code:193',4,94,2),(194,'code:194',5,95,1),(195,'code:195',6,96,2),(196,'code:196',7,97,1),(197,'code:197',8,98,2),(198,'code:198',9,99,1),(199,'code:199',10,100,2),(200,'code:200',1,1,1);
/*!40000 ALTER TABLE `registration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student`
--

DROP TABLE IF EXISTS `student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `student` (
  `student_id` int NOT NULL AUTO_INCREMENT,
  `date_of_birth` datetime DEFAULT NULL,
  `disabled` bit(1) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `student_code` varchar(255) NOT NULL,
  `student_email` varchar(255) NOT NULL,
  `student_image_id` int DEFAULT NULL,
  PRIMARY KEY (`student_id`),
  KEY `FKfhj45y7j19a3avmqr00g590m4` (`student_image_id`),
  CONSTRAINT `FKfhj45y7j19a3avmqr00g590m4` FOREIGN KEY (`student_image_id`) REFERENCES `student_image` (`student_image_id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student`
--

LOCK TABLES `student` WRITE;
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
INSERT INTO `student` VALUES (1,'2000-03-01 00:00:00',_binary '\0','fname1','lname1','code: 1','example1@456.com',NULL),(2,'2000-03-01 00:00:00',_binary '\0','fname2','lname2','code: 2','example2@456.com',NULL),(3,'2000-03-01 00:00:00',_binary '\0','fname3','lname3','code: 3','example3@456.com',NULL),(4,'2000-03-01 00:00:00',_binary '\0','fname4','lname4','code: 4','example4@456.com',NULL),(5,'2000-03-01 00:00:00',_binary '\0','fname5','lname5','code: 5','example5@456.com',NULL),(6,'2000-03-01 00:00:00',_binary '\0','fname6','lname6','code: 6','example6@456.com',NULL),(7,'2000-03-01 00:00:00',_binary '\0','fname7','lname7','code: 7','example7@456.com',NULL),(8,'2000-03-01 00:00:00',_binary '\0','fname8','lname8','code: 8','example8@456.com',NULL),(9,'2000-03-01 00:00:00',_binary '\0','fname9','lname9','code: 9','example9@456.com',NULL),(10,'2000-03-01 00:00:00',_binary '\0','fname10','lname10','code: 10','example10@456.com',NULL),(11,'2000-03-01 00:00:00',_binary '\0','fname11','lname11','code: 11','example11@456.com',NULL),(12,'2000-03-01 00:00:00',_binary '\0','fname12','lname12','code: 12','example12@456.com',NULL),(13,'2000-03-01 00:00:00',_binary '\0','fname13','lname13','code: 13','example13@456.com',NULL),(14,'2000-03-01 00:00:00',_binary '\0','fname14','lname14','code: 14','example14@456.com',NULL),(15,'2000-03-01 00:00:00',_binary '\0','fname15','lname15','code: 15','example15@456.com',NULL),(16,'2000-03-01 00:00:00',_binary '\0','fname16','lname16','code: 16','example16@456.com',NULL),(17,'2000-03-01 00:00:00',_binary '\0','fname17','lname17','code: 17','example17@456.com',NULL),(18,'2000-03-01 00:00:00',_binary '\0','fname18','lname18','code: 18','example18@456.com',NULL),(19,'2000-03-01 00:00:00',_binary '\0','fname19','lname19','code: 19','example19@456.com',NULL),(20,'2000-03-01 00:00:00',_binary '\0','fname20','lname20','code: 20','example20@456.com',NULL),(21,'2000-03-01 00:00:00',_binary '\0','fname21','lname21','code: 21','example21@456.com',NULL),(22,'2000-03-01 00:00:00',_binary '\0','fname22','lname22','code: 22','example22@456.com',NULL),(23,'2000-03-01 00:00:00',_binary '\0','fname23','lname23','code: 23','example23@456.com',NULL),(24,'2000-03-01 00:00:00',_binary '\0','fname24','lname24','code: 24','example24@456.com',NULL),(25,'2000-03-01 00:00:00',_binary '\0','fname25','lname25','code: 25','example25@456.com',NULL),(26,'2000-03-01 00:00:00',_binary '\0','fname26','lname26','code: 26','example26@456.com',NULL),(27,'2000-03-01 00:00:00',_binary '\0','fname27','lname27','code: 27','example27@456.com',NULL),(28,'2000-03-01 00:00:00',_binary '\0','fname28','lname28','code: 28','example28@456.com',NULL),(29,'2000-03-01 00:00:00',_binary '\0','fname29','lname29','code: 29','example29@456.com',NULL),(30,'2000-03-01 00:00:00',_binary '\0','fname30','lname30','code: 30','example30@456.com',NULL),(31,'2000-03-01 00:00:00',_binary '\0','fname31','lname31','code: 31','example31@456.com',NULL),(32,'2000-03-01 00:00:00',_binary '\0','fname32','lname32','code: 32','example32@456.com',NULL),(33,'2000-03-01 00:00:00',_binary '\0','fname33','lname33','code: 33','example33@456.com',NULL),(34,'2000-03-01 00:00:00',_binary '\0','fname34','lname34','code: 34','example34@456.com',NULL),(35,'2000-03-01 00:00:00',_binary '\0','fname35','lname35','code: 35','example35@456.com',NULL),(36,'2000-03-01 00:00:00',_binary '\0','fname36','lname36','code: 36','example36@456.com',NULL),(37,'2000-03-01 00:00:00',_binary '\0','fname37','lname37','code: 37','example37@456.com',NULL),(38,'2000-03-01 00:00:00',_binary '\0','fname38','lname38','code: 38','example38@456.com',NULL),(39,'2000-03-01 00:00:00',_binary '\0','fname39','lname39','code: 39','example39@456.com',NULL),(40,'2000-03-01 00:00:00',_binary '\0','fname40','lname40','code: 40','example40@456.com',NULL),(41,'2000-03-01 00:00:00',_binary '\0','fname41','lname41','code: 41','example41@456.com',NULL),(42,'2000-03-01 00:00:00',_binary '\0','fname42','lname42','code: 42','example42@456.com',NULL),(43,'2000-03-01 00:00:00',_binary '\0','fname43','lname43','code: 43','example43@456.com',NULL),(44,'2000-03-01 00:00:00',_binary '\0','fname44','lname44','code: 44','example44@456.com',NULL),(45,'2000-03-01 00:00:00',_binary '\0','fname45','lname45','code: 45','example45@456.com',NULL),(46,'2000-03-01 00:00:00',_binary '\0','fname46','lname46','code: 46','example46@456.com',NULL),(47,'2000-03-01 00:00:00',_binary '\0','fname47','lname47','code: 47','example47@456.com',NULL),(48,'2000-03-01 00:00:00',_binary '\0','fname48','lname48','code: 48','example48@456.com',NULL),(49,'2000-03-01 00:00:00',_binary '\0','fname49','lname49','code: 49','example49@456.com',NULL),(50,'2000-03-01 00:00:00',_binary '\0','fname50','lname50','code: 50','example50@456.com',NULL),(51,'2000-03-01 00:00:00',_binary '\0','fname51','lname51','code: 51','example51@456.com',NULL),(52,'2000-03-01 00:00:00',_binary '\0','fname52','lname52','code: 52','example52@456.com',NULL),(53,'2000-03-01 00:00:00',_binary '\0','fname53','lname53','code: 53','example53@456.com',NULL),(54,'2000-03-01 00:00:00',_binary '\0','fname54','lname54','code: 54','example54@456.com',NULL),(55,'2000-03-01 00:00:00',_binary '\0','fname55','lname55','code: 55','example55@456.com',NULL),(56,'2000-03-01 00:00:00',_binary '\0','fname56','lname56','code: 56','example56@456.com',NULL),(57,'2000-03-01 00:00:00',_binary '\0','fname57','lname57','code: 57','example57@456.com',NULL),(58,'2000-03-01 00:00:00',_binary '\0','fname58','lname58','code: 58','example58@456.com',NULL),(59,'2000-03-01 00:00:00',_binary '\0','fname59','lname59','code: 59','example59@456.com',NULL),(60,'2000-03-01 00:00:00',_binary '\0','fname60','lname60','code: 60','example60@456.com',NULL),(61,'2000-03-01 00:00:00',_binary '\0','fname61','lname61','code: 61','example61@456.com',NULL),(62,'2000-03-01 00:00:00',_binary '\0','fname62','lname62','code: 62','example62@456.com',NULL),(63,'2000-03-01 00:00:00',_binary '\0','fname63','lname63','code: 63','example63@456.com',NULL),(64,'2000-03-01 00:00:00',_binary '\0','fname64','lname64','code: 64','example64@456.com',NULL),(65,'2000-03-01 00:00:00',_binary '\0','fname65','lname65','code: 65','example65@456.com',NULL),(66,'2000-03-01 00:00:00',_binary '\0','fname66','lname66','code: 66','example66@456.com',NULL),(67,'2000-03-01 00:00:00',_binary '\0','fname67','lname67','code: 67','example67@456.com',NULL),(68,'2000-03-01 00:00:00',_binary '\0','fname68','lname68','code: 68','example68@456.com',NULL),(69,'2000-03-01 00:00:00',_binary '\0','fname69','lname69','code: 69','example69@456.com',NULL),(70,'2000-03-01 00:00:00',_binary '\0','fname70','lname70','code: 70','example70@456.com',NULL),(71,'2000-03-01 00:00:00',_binary '\0','fname71','lname71','code: 71','example71@456.com',NULL),(72,'2000-03-01 00:00:00',_binary '\0','fname72','lname72','code: 72','example72@456.com',NULL),(73,'2000-03-01 00:00:00',_binary '\0','fname73','lname73','code: 73','example73@456.com',NULL),(74,'2000-03-01 00:00:00',_binary '\0','fname74','lname74','code: 74','example74@456.com',NULL),(75,'2000-03-01 00:00:00',_binary '\0','fname75','lname75','code: 75','example75@456.com',NULL),(76,'2000-03-01 00:00:00',_binary '\0','fname76','lname76','code: 76','example76@456.com',NULL),(77,'2000-03-01 00:00:00',_binary '\0','fname77','lname77','code: 77','example77@456.com',NULL),(78,'2000-03-01 00:00:00',_binary '\0','fname78','lname78','code: 78','example78@456.com',NULL),(79,'2000-03-01 00:00:00',_binary '\0','fname79','lname79','code: 79','example79@456.com',NULL),(80,'2000-03-01 00:00:00',_binary '\0','fname80','lname80','code: 80','example80@456.com',NULL),(81,'2000-03-01 00:00:00',_binary '\0','fname81','lname81','code: 81','example81@456.com',NULL),(82,'2000-03-01 00:00:00',_binary '\0','fname82','lname82','code: 82','example82@456.com',NULL),(83,'2000-03-01 00:00:00',_binary '\0','fname83','lname83','code: 83','example83@456.com',NULL),(84,'2000-03-01 00:00:00',_binary '\0','fname84','lname84','code: 84','example84@456.com',NULL),(85,'2000-03-01 00:00:00',_binary '\0','fname85','lname85','code: 85','example85@456.com',NULL),(86,'2000-03-01 00:00:00',_binary '\0','fname86','lname86','code: 86','example86@456.com',NULL),(87,'2000-03-01 00:00:00',_binary '\0','fname87','lname87','code: 87','example87@456.com',NULL),(88,'2000-03-01 00:00:00',_binary '\0','fname88','lname88','code: 88','example88@456.com',NULL),(89,'2000-03-01 00:00:00',_binary '\0','fname89','lname89','code: 89','example89@456.com',NULL),(90,'2000-03-01 00:00:00',_binary '\0','fname90','lname90','code: 90','example90@456.com',NULL),(91,'2000-03-01 00:00:00',_binary '\0','fname91','lname91','code: 91','example91@456.com',NULL),(92,'2000-03-01 00:00:00',_binary '\0','fname92','lname92','code: 92','example92@456.com',NULL),(93,'2000-03-01 00:00:00',_binary '\0','fname93','lname93','code: 93','example93@456.com',NULL),(94,'2000-03-01 00:00:00',_binary '\0','fname94','lname94','code: 94','example94@456.com',NULL),(95,'2000-03-01 00:00:00',_binary '\0','fname95','lname95','code: 95','example95@456.com',NULL),(96,'2000-03-01 00:00:00',_binary '\0','fname96','lname96','code: 96','example96@456.com',NULL),(97,'2000-03-01 00:00:00',_binary '\0','fname97','lname97','code: 97','example97@456.com',NULL),(98,'2000-03-01 00:00:00',_binary '\0','fname98','lname98','code: 98','example98@456.com',NULL),(99,'2000-03-01 00:00:00',_binary '\0','fname99','lname99','code: 99','example99@456.com',NULL),(100,'2000-03-01 00:00:00',_binary '\0','fname100','lname100','code: 100','example100@456.com',NULL);
/*!40000 ALTER TABLE `student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_image`
--

DROP TABLE IF EXISTS `student_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `student_image` (
  `student_image_id` int NOT NULL AUTO_INCREMENT,
  `student_image_file` varchar(255),
  `student_image_code` varchar(255) NOT NULL,
  PRIMARY KEY (`student_image_id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_image`
--

LOCK TABLES `student_image` WRITE;
/*!40000 ALTER TABLE `student_image` DISABLE KEYS */;
INSERT INTO `student_image` VALUES (1,NULL,'code: 1'),(2,NULL,'code: 2'),(3,NULL,'code: 3'),(4,NULL,'code: 4'),(5,NULL,'code: 5'),(6,NULL,'code: 6'),(7,NULL,'code: 7'),(8,NULL,'code: 8'),(9,NULL,'code: 9'),(10,NULL,'code: 10'),(11,NULL,'code: 11'),(12,NULL,'code: 12'),(13,NULL,'code: 13'),(14,NULL,'code: 14'),(15,NULL,'code: 15'),(16,NULL,'code: 16'),(17,NULL,'code: 17'),(18,NULL,'code: 18'),(19,NULL,'code: 19'),(20,NULL,'code: 20'),(21,NULL,'code: 21'),(22,NULL,'code: 22'),(23,NULL,'code: 23'),(24,NULL,'code: 24'),(25,NULL,'code: 25'),(26,NULL,'code: 26'),(27,NULL,'code: 27'),(28,NULL,'code: 28'),(29,NULL,'code: 29'),(30,NULL,'code: 30'),(31,NULL,'code: 31'),(32,NULL,'code: 32'),(33,NULL,'code: 33'),(34,NULL,'code: 34'),(35,NULL,'code: 35'),(36,NULL,'code: 36'),(37,NULL,'code: 37'),(38,NULL,'code: 38'),(39,NULL,'code: 39'),(40,NULL,'code: 40'),(41,NULL,'code: 41'),(42,NULL,'code: 42'),(43,NULL,'code: 43'),(44,NULL,'code: 44'),(45,NULL,'code: 45'),(46,NULL,'code: 46'),(47,NULL,'code: 47'),(48,NULL,'code: 48'),(49,NULL,'code: 49'),(50,NULL,'code: 50'),(51,NULL,'code: 51'),(52,NULL,'code: 52'),(53,NULL,'code: 53'),(54,NULL,'code: 54'),(55,NULL,'code: 55'),(56,NULL,'code: 56'),(57,NULL,'code: 57'),(58,NULL,'code: 58'),(59,NULL,'code: 59'),(60,NULL,'code: 60'),(61,NULL,'code: 61'),(62,NULL,'code: 62'),(63,NULL,'code: 63'),(64,NULL,'code: 64'),(65,NULL,'code: 65'),(66,NULL,'code: 66'),(67,NULL,'code: 67'),(68,NULL,'code: 68'),(69,NULL,'code: 69'),(70,NULL,'code: 70'),(71,NULL,'code: 71'),(72,NULL,'code: 72'),(73,NULL,'code: 73'),(74,NULL,'code: 74'),(75,NULL,'code: 75'),(76,NULL,'code: 76'),(77,NULL,'code: 77'),(78,NULL,'code: 78'),(79,NULL,'code: 79'),(80,NULL,'code: 80'),(81,NULL,'code: 81'),(82,NULL,'code: 82'),(83,NULL,'code: 83'),(84,NULL,'code: 84'),(85,NULL,'code: 85'),(86,NULL,'code: 86'),(87,NULL,'code: 87'),(88,NULL,'code: 88'),(89,NULL,'code: 89'),(90,NULL,'code: 90'),(91,NULL,'code: 91'),(92,NULL,'code: 92'),(93,NULL,'code: 93'),(94,NULL,'code: 94'),(95,NULL,'code: 95'),(96,NULL,'code: 96'),(97,NULL,'code: 97'),(98,NULL,'code: 98'),(99,NULL,'code: 99'),(100,NULL,'code: 100');
/*!40000 ALTER TABLE `student_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teacher`
--

DROP TABLE IF EXISTS `teacher`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `teacher` (
  `teacher_id` int NOT NULL AUTO_INCREMENT,
  `disabled` bit(1) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `teacher_code` varchar(255) NOT NULL,
  `teacher_email` varchar(255) NOT NULL,
  `department_id` int DEFAULT NULL,
  PRIMARY KEY (`teacher_id`),
  KEY `FKd419q6obhj46eoye136y7rjyq` (`department_id`),
  CONSTRAINT `FKd419q6obhj46eoye136y7rjyq` FOREIGN KEY (`department_id`) REFERENCES `department` (`department_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teacher`
--

LOCK TABLES `teacher` WRITE;
/*!40000 ALTER TABLE `teacher` DISABLE KEYS */;
INSERT INTO `teacher` VALUES (1,_binary '\0','fname:1','lname:1','code:1','example1@456.com',2),(2,_binary '\0','fname:2','lname:2','code:2','example2@456.com',3),(3,_binary '\0','fname:3','lname:3','code:3','example3@456.com',4),(4,_binary '\0','fname:4','lname:4','code:4','example4@456.com',5),(5,_binary '\0','fname:5','lname:5','code:5','example5@456.com',6),(6,_binary '\0','fname:6','lname:6','code:6','example6@456.com',7),(7,_binary '\0','fname:7','lname:7','code:7','example7@456.com',8),(8,_binary '\0','fname:8','lname:8','code:8','example8@456.com',9),(9,_binary '\0','fname:9','lname:9','code:9','example9@456.com',10),(10,_binary '\0','fname:10','lname:10','code:10','example10@456.com',1);
/*!40000 ALTER TABLE `teacher` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `term`
--

DROP TABLE IF EXISTS `term`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `term` (
  `term_id` int NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  `disabled` bit(1) NOT NULL,
  `term_code` varchar(255) NOT NULL,
  PRIMARY KEY (`term_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `term`
--

LOCK TABLES `term` WRITE;
/*!40000 ALTER TABLE `term` DISABLE KEYS */;
INSERT INTO `term` VALUES (1,'description:1',_binary '\0','code:1'),(2,'description:2',_binary '\0','code:2');
/*!40000 ALTER TABLE `term` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-03-20 19:00:31
