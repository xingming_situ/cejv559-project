package com.concordia.project.email.service;
import com.concordia.project.email.model.ContactForm;
import com.concordia.project.exception.EmailException;

/**
 * @author Tetiana
 *
 */
public interface EmailService {
	
	/**
	 * Send email through 
	 *
	 * @param emailAddressFrom The addressFrom
	 * @param emailAddressTo The addressTo
	 * @param subject The subject of the email.
	 * @param message The message content in the body which regular text.
	 * @throws EmailException
	 */
	boolean sendEmail(String emailAddressFrom, String emailAddressTo, String subject, String messageContent) throws EmailException;
	
	/**
	 * Send email through 
	 *
	 * @param emailName The name from the contact us form
	 * @param emailSender The address of the sender
	 * @param messageContent The message content in the body which regular text.
	 * @throws EmailException
	 */
	boolean contactUs(ContactForm contactForm) throws EmailException;

}