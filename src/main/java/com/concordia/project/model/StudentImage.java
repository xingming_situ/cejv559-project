package com.concordia.project.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;

/**
 * CEJV 559 - Group2 Project
 * Registration is a model for the registration table
 * Model has REgistration class with fields, constructors, getters, setters, toString
 */
/**
 * @author Tetiana, Marchiano
 */
@Entity
@Table(name = "student_image")
public class StudentImage {
	
	private int studentImageId;
	
	@NotEmpty(message = "Student Image code cannot be empty")
	private String studentImageCode;
	private String studentImageFile;
	
		
	/**
	 * Empty constructor
	 */
	public StudentImage() {
		// do nothing
	}

	/**Default constructor
	 * @param studentImageCode
	 * @param studentImageFile
	 */
	public StudentImage(String studentImageCode, String studentImageFile) {
		super();
		this.studentImageCode = studentImageCode;
		this.studentImageFile = studentImageFile;
	}

	// Getters, setters
	/**
	 * @return the studentImageId
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "student_image_id")
	public int getStudentImageId() {
		return studentImageId;
	}

	/**
	 * @param studentImageId the studentImageId to set
	 */
	public void setStudentImageId(int studentImageId) {
		this.studentImageId = studentImageId;
	}

	/**
	 * @return the studentImageCode
	 */
	@Column(name = "student_image_code", nullable = true)
	public String getStudentImageCode() {
		return studentImageCode;
	}

	/**
	 * @param studentImageCode the studentImageCode to set
	 */
	public void setStudentImageCode(String studentImageCode) {
		this.studentImageCode = studentImageCode;
	}
	
	/**
	 * @return the studentImageFile
	 */
	@Column(name = "student_image_file", nullable = true)
	public String getStudentImageFile() {
		return studentImageFile;
	}

	/**
	 * @param studentImageFile the studentImageFile to set
	 */
	public void setStudentImageFile(String studentImageFile) {
		this.studentImageFile = studentImageFile;
	}

	/**
	 * @return student image path
	 */
	@Transient
    public String getStudentImageFileImagePath() {
        if (studentImageFile == null) return null;
         
        return "/student-images/" + studentImageId + "/" + studentImageFile;
    }
		

	/**
	 * toString method for StudentImage class
	 * 
	 * @return All of the attributes of a StudentImage object in one String
	 */

	
	@Override
	public String toString() {
		return "StudentImage [studentImageId=" + studentImageId + ", studentImageFile="
				+ studentImageFile + ", studentImageCode=" + studentImageCode + "]";
	}

}
