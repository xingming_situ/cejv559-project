package com.concordia.project.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

/**
 * CEJV 559 - Group2 Project
 * Registration is a model for the registration table
 * Model has REgistration class with fields, constructors, getters, setters, toString
 */
/**
 * @author Tetiana
 */
@Entity
@Table(name = "registration")
public class Registration {

	private int registrationId;
	
	@NotEmpty(message = "Registration code cannot be empty")
	private String registrationCode;

	// many to one mapping with student table
	private Student student;

	// many to one mapping with course table
	private Course course;

	// many to one mapping with course table
	private Term term;

	/**
	 * Empty constructor
	 */
	public Registration() {
		// do nothing
	}

	/**
	 * Default constructor that takes all columns
	 * 
	 * @param registrationCode
	 * @param student
	 * @param course
	 * @param term
	 */
	public Registration(String registrationCode) {
		super();
		this.registrationCode = registrationCode;
	}
	
	

	// Getters, setters

	/**
	 * Constructor for the registration with corresponding student, course and teacher objects
	 * @param registrationId
	 * @param registrationCode
	 * @param student
	 * @param course
	 * @param term
	 */
	public Registration(int registrationId,
			@NotEmpty(message = "Registration code cannot be empty") String registrationCode, Student student,
			Course course, Term term) {
		super();
		this.registrationId = registrationId;
		this.registrationCode = registrationCode;
		this.student = student;
		this.course = course;
		this.term = term;
	}

	/**
	 * @return the registrationId
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "registration_id", nullable = false)
	public int getRegistrationId() {
		return registrationId;
	}

	/**
	 * @param registrationId the registrationId to set
	 */
	public void setRegistrationId(int registrationId) {
		this.registrationId = registrationId;
	}

	/**
	 * @return the registrationCode
	 */
	@Column(name = "registration_code", nullable = false)
	public String getRegistrationCode() {
		return registrationCode;
	}

	/**
	 * @param registrationCode the registrationCode to set
	 */
	public void setRegistrationCode(String registrationCode) {
		this.registrationCode = registrationCode;
	}

	/**
	 * @return the student
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "student_id")
	public Student getStudent() {
		return student;
	}

	/**
	 * @param student the student to set
	 */
	public void setStudent(Student student) {
		this.student = student;
	}

	/**
	 * @return the course
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "course_id")
	public Course getCourse() {
		return course;
	}

	/**
	 * @param course the course to set
	 */
	public void setCourse(Course course) {
		this.course = course;
	}

	/**
	 * @return the term
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "term_id")
	public Term getTerm() {
		return term;
	}

	/**
	 * @param term the term to set
	 */
	public void setTerm(Term term) {
		this.term = term;
	}

	/**
	 * @return the auto generated registrationCode
	 */
	/*private String generateCode() {
		return ("Registration: " + "XXX");
	}*/

	/**
	 * toString method for Registration class
	 * 
	 * @return All of the attributes of a Registration object in one String
	 */
	@Override
	public String toString() {
		return "Registration [registrationId=" + registrationId + ", registrationCode=" + registrationCode
				+ ", student=" + student + ", course=" + course + ", term=" + term + "]";
	}

}
