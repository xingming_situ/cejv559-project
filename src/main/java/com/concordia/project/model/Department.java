package com.concordia.project.model;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * CEJV 559 - Group2 Project
 * Department is a model for the department table
 * Model has Department class with fields, constructors, getters, setters, toString
 */
/**
 * @author Elliott
 */

@Entity
@Table(name = "department")
public class Department {

	private int departmentId;
	
	@NotEmpty(message = "Department code cannot be empty")
	private String departmentCode;
	
	@NotEmpty(message = "Department description cannot be empty")
	private String description;
	
	@NotNull(message = "Status must be specified")
	private boolean disabled;
	
	// One to many mapping

	private Set<Teacher> teachers = new HashSet<Teacher>();

	/**
	 * Empty constructor
	 */
	public Department() {
		// do nothing
	}

	@Column(name = "department_code", nullable = false)
	public String getDepartmentCode() {

		return this.departmentCode;
	}

	/**
	 * @param departmentCode the departmentCode to set
	 */
	public void setDepartmentCode(String departmentCode) {
		this.departmentCode = departmentCode;
	}

	/**
	 * Default constructor that takes the mandatory columns of department
	 * 
	 * @param description
	 */
	public Department(String description) {
		super();

		this.setDescription(description);
		this.departmentCode = generateCode();
		this.disabled = false;
	}

	private String generateCode() {

		return (this.description.toLowerCase());
	}

	// Getters, setters, and toString
	/**
	 * @return the departmentId
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "department_id")
	public int getDepartmentId() {
		return departmentId;
	}

	/**
	 * @return the description
	 */
	@Column(name = "description", nullable = false)
	public String getDescription() {
		return description;
	}

	/**
	 * @param departmentId the departmentId to set
	 */
	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}

	/**
	 * Set the department description
	 * 
	 * @param description
	 */
	public void setDescription(String description) {
		if (description.length() == 0) {
			this.description = description + "XXX";
		}

		else {
			this.description = description;
		}
	}

	/**
	 * @return the disabled
	 */
	@Column(name = "disabled")
	public boolean getDisabled() {
		return disabled;
	}

	/**
	 * Set the department status
	 * 
	 * @param disabled the disabled to set
	 */
	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	/**
	 * One to many mapping with teachers table
	 * 
	 * @return the teachers
	 */
	@OneToMany(mappedBy = "department", cascade = CascadeType.ALL)
	public Set<Teacher> getTeachers() {
		return teachers;
	}

	/**
	 * @param teachers the teachers to set
	 */
	public void setTeachers(Set<Teacher> teachers) {
		this.teachers = teachers;
	}


	/**
	 * toString method for Department class
	 *
	 * @return All of the attributes of a Department object in one String
	 */
	@Override
	public String toString() {
		return "Department [departmentId=" + departmentId + ", departmentCode=" + departmentCode + ", description="
				+ description + ", disabled=" + disabled + ", teachers=" + teachers + "]";
	}

}
