package com.concordia.project.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;

import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Transient;

/**
 * CEJV 559 - Group2 Project
 * Course is a model for the course table
 * Model has Course class with fields, constructors, getters, setters, toString
 */
/**
 * @author Elliott
 */

@Entity
@Table(name = "course")
public class Course {

	private int courseId;
	
	@NotEmpty(message = "Course description cannot be empty")
	private String description;
	
	@NotNull(message = "Course cost must be valid")
	private double courseCost;
	
	@NotNull(message = "Status must be specified")
	private boolean disabled;
	
	@NotEmpty(message = "Course code cannot be empty")
	private String courseCode;

	// Many to one mapping with teacher table
	private Teacher teacher = null;

	// One to many mapping with registration table
	private Set<Registration> registration = new HashSet<Registration>();

	/**
	 * Empty constructor
	 */
	public Course() {
		// do nothing
	}

	/**
	 * @return the courseCode
	 */
	private String generateCode() {

		return (this.description.toLowerCase());
	}

	/**
	 * Default constructor that takes the mandatory columns of course
	 * 
	 * @param courseId
	 * @param description
	 * @param disabled
	 */
	public Course(String description, double courseCost) {
		super();
		this.setDescription(description);
		this.disabled = false;
		this.courseCode = generateCode();
		this.setCourseCost(courseCost);
	}

// Getters, setters, and toString
	/**
	 * @return the courseId
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "course_id")
	public int getCourseId() {
		return courseId;
	}

	@Transient
	@Column(name = "course_code", nullable = false)
	public String getcourseCode() {

		return this.courseCode;
	}

	/**
	 * @param courseCode the courseCode to set
	 */
	public void setCourseCode(String courseCode) {
		this.courseCode = courseCode;
	}

	/**
	 * @param courseId the courseId to set
	 */
	public void setCourseId(int courseId) {
		this.courseId = courseId;
	}

	/**
	 * @return the description
	 */
	@Column(name = "description", nullable = false)
	public String getDescription() {
		return description;
	}

	/**
	 * Set the course description
	 * 
	 * @param description
	 */
	public void setDescription(String description) {

		if (description.length() == 0) {
			this.description = description + "XXX";
		}

		else {

			this.description = description;
		}

	}

	/**
	 * @return the disabled
	 */
	@Column(name = "disabled")
	public boolean getDisabled() {
		return disabled;
	}

	/**
	 * Set the course status
	 * 
	 * @param disabled
	 */
	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	/**
	 * @return the disabled
	 */
	@Column(name = "course_cost")
	public double getCourseCost() {
		return courseCost;
	}
	
	/**
	 * @param courseCost the courseCost to set
	 */
	public void setCourseCost(double courseCost) {

		if (courseCost < 0) {
			this.courseCost = 0.0;
		}

		else {
			this.courseCost = courseCost;
		}
	}
	
	/**
	 * @return the corresponding teacher
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "teacher_id")
	public Teacher getTeacher() {
		return teacher;
	}

	/**
	 * Set the corresponding teacher
	 * 
	 * @param department
	 */
	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}

	
	/**
	 * @return the registration
	 */
	@OneToMany(mappedBy = "course", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public Set<Registration> getRegistration() {
		return registration;
	}

	/**
	 * @param registration the registration to set
	 */
	public void setRegistration(Set<Registration> registration) {
		this.registration = registration;
	}

	/**
	 * toString method for Course class
	 *
	 * @return All of the attributes of a Course object in one String
	 */
	@Override
	public String toString() {
		return "Course [courseId=" + courseId + ", description=" + description + ", courseCost=" + courseCost
				+ ", disabled=" + disabled + ", courseCode=" + courseCode + ", teacher=" + teacher + ", registration="
				+ registration + "]";
	}

}
