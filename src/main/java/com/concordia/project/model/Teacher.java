package com.concordia.project.model;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * CEJV 559 - Group2 Project
 * Teacher is a model for the teacher table
 * Model has Teacher class with fields, constructors, getters, setters, toString
 */
/**
 * @author Marchiano, Tetiana, Elliott
 */

@Entity
@Table(name = "teacher")
public class Teacher {

	private int teacherId;
	
	@NotEmpty(message = "Teacher code cannot be empty")
	private String teacherCode;
	
	@NotEmpty(message = "First name cannot be empty")
	@Size(min = 2, max = 35, message = "First name must be between 2 and 35 characters")
	private String firstName;
	
	@NotEmpty(message = "Last name cannot be empty")
	@Size(min = 2, max = 35, message = "Last name must be between 2 and 35 characters")
	private String lastName;
	
	@NotEmpty(message = "Teacher email cannot be empty")
	@Email(message = "Teacher email must be a valid email address")
	private String teacherEmail;
	
	@NotNull(message = "Status must be specified")
	private boolean disabled;
	
	// Many to one mapping with department table
	private Department department = null;
	// One to many mapping with course table
	private Set<Course> courses = new HashSet<Course>();

	/**
	 * Empty constructor
	 */
	public Teacher() {
		// do nothing
	}

	/**
	 * @param teacherId the teacherId to set
	 */
	public void setTeacherId(int teacherId) {
		this.teacherId = teacherId;
	}

	/**
	 * Default constructor that takes in columns in Teacher table
	 * 
	 * @param teacherCode
	 * @param firstName
	 * @param lastName
	 * @param teacherEmail
	 * @param disabled
	 */
	public Teacher(String firstName, String lastName, String teacherEmail) {
		this.teacherCode = generateCode();
		this.firstName = firstName;
		this.lastName = lastName;
		this.teacherEmail = teacherEmail;
		this.disabled = false;

	}

	// Getters, setters, and toString
	/**
	 * @return the teacher ID
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getTeacherId() {
		return teacherId;
	}

	/**
	 * @param teacherCode the teacherCode to set
	 */
	public void setTeacherCode(String teacherCode) {
		this.teacherCode = teacherCode;
	}

	/**
	 * @return the code of the teacher
	 */
	@Column(name = "teacher_code", nullable = false)
	public String getTeacherCode() {
		return teacherCode;
	}

	private String generateCode() {

		return ("Teacher: " + this.teacherId);
	}

	/**
	 * @return the first name of the teacher
	 */
	@Column(name = "first_name", nullable = false)
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Set the first name of the teacher
	 * 
	 * @param firstName
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the last name of the teacher
	 */
	@Column(name = "last_name", nullable = false)
	public String getLastName() {
		return lastName;
	}

	/**
	 * Set the last name of the teacher
	 * 
	 * @param lastName
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return teacher's email
	 */
	@Column(name = "teacher_email", nullable = false)
	public String getTeacherEmail() {
		return teacherEmail;
	}

	/**
	 * Set the email of the teacher
	 * 
	 * @param teacherEmail
	 */
	public void setTeacherEmail(String teacherEmail) {
		this.teacherEmail = teacherEmail;
	}

	/**
	 * @return teacher's status
	 */
	@Column(name = "disabled", nullable = false)
	public boolean getDisabled() {
		return disabled;
	}

	/**
	 * Set teacher's status
	 * 
	 * @param disabled
	 */
	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	/**
	 * @return the corresponding department
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "department_id")
	public Department getDepartment() {
		return department;
	}

	/**
	 * @param department the department to set
	 */
	public void setDepartment(Department department) {
		this.department = department;
	}

	/**
	 * One to many mapping with course table
	 * 
	 * @return the courses
	 */
	@OneToMany(mappedBy = "teacher", cascade = CascadeType.ALL)
	public Set<Course> getCourses() {
		return courses;
	}

	/**
	 * @param courses the courses to set
	 */
	public void setCourses(Set<Course> courses) {
		this.courses = courses;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((courses == null) ? 0 : courses.hashCode());
		result = prime * result + ((department == null) ? 0 : department.hashCode());
		result = prime * result + (disabled ? 1231 : 1237);
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((teacherCode == null) ? 0 : teacherCode.hashCode());
		result = prime * result + ((teacherEmail == null) ? 0 : teacherEmail.hashCode());
		result = prime * result + (int) (teacherId ^ (teacherId >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Teacher other = (Teacher) obj;
		if (courses == null) {
			if (other.courses != null)
				return false;
		} else if (!courses.equals(other.courses))
			return false;
		if (department == null) {
			if (other.department != null)
				return false;
		} else if (!department.equals(other.department))
			return false;
		if (disabled != other.disabled)
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (teacherCode == null) {
			if (other.teacherCode != null)
				return false;
		} else if (!teacherCode.equals(other.teacherCode))
			return false;
		if (teacherEmail == null) {
			if (other.teacherEmail != null)
				return false;
		} else if (!teacherEmail.equals(other.teacherEmail))
			return false;
		if (teacherId != other.teacherId)
			return false;
		return true;
	}

	/**
	 * toString method for Teacher class
	 * 
	 * @return All of the attributes of a Teacher object in one String
	 */

	@Override
	public String toString() {
		return "Teacher [teacherId=" + teacherId + ", teacherCode=" + teacherCode + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", teacherEmail=" + teacherEmail + ", disabled=" + disabled
				+ ", department=" + department + ", courses=" + courses + "]";
	}

}
