package com.concordia.project.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.concordia.project.dto.CourseDTO;
import com.concordia.project.exception.ServiceException;
import com.concordia.project.service.CourseService;

/**
 * CEJV 559 - Group 2 Project
 * Controller for Course
 * /**
 *
 * @author Marchiano
 */

@Controller
public class CourseController {

	@Autowired
	private CourseService courseService;

	/**
	 * Page to display records
	 * @param model
	 * @return page of records sorted by courseId
	 */
	@GetMapping("/courses")
	public String viewHomePage(Model model)  throws ServiceException{
		return findPaginated(1, "courseId", "asc", model);
	}

	/**
	 * Add new course form page
	 * @param model
	 * @return new course page which has the form to add new course 
	 */
	@GetMapping({ "/showNewCourseForm" })
	public String showNewCourseForm(Model model) {
		// create model attribute to bind form data
		CourseDTO  courseDTO = new CourseDTO();
		model.addAttribute("course", courseDTO);
		return "new_course";
	}

	/**
	 * Save course
	 * @param courseDTO
	 * @return page with course records with new course added
	 * @throws ServiceException if course cannot be saved
	 */
	@PostMapping("/saveCourse")
	public String saveCourse(@ModelAttribute("course") CourseDTO courseDTO) throws ServiceException {
		courseService.saveCourse(courseDTO);
		return "redirect:/courses";
	}
	
	/**
	 * Update course
	 * @param courseId
	 * @param model
	 * @return update course page which has the form to update existing course
	 * @throws ServiceException if course cannot be updated
	 */
	@GetMapping("/showUpdateCourseForm/{courseId}")
	public String showUpdateCourseForm(@PathVariable ( value = "courseId") Integer courseId, Model model) throws ServiceException {
		
		CourseDTO courseDTO = courseService.getCourse(courseId);
		
		// set course as a model attribute to pre-populate the form
		model.addAttribute("course", courseDTO);
		return "update_course";
	}
	
	/**
	 * Delete course
	 * @param courseId
	 * @return page with course records with course of interest deleted
	 * @throws ServiceException if course cannot be deleted
	 */
	@GetMapping("/deleteCourse/{courseId}")
	public String deleteCourse(@PathVariable (value = "courseId") int courseId) throws ServiceException {
		
		this.courseService.deleteCourse(courseId);
		return "redirect:/courses";
	}

	/**
	 * Pagination for courses page
	 * @param pageNo
	 * @param sortField
	 * @param sortDir
	 * @param model
	 * @return paginated course records page
	 */
	@GetMapping("pageCourse/{pageNo}")
	public String findPaginated(@PathVariable (value = "pageNo") int pageNo, 
			@RequestParam("sortField") String sortField,
			@RequestParam("sortDir") String sortDir,
			Model model) {
		
		int pageSize = 5;
		
		Page<CourseDTO> page = courseService.findPaginated(pageNo, pageSize, sortField, sortDir);
		List<CourseDTO> listCourses = page.getContent();
		
		model.addAttribute("currentPage", pageNo);
		model.addAttribute("totalPages", page.getTotalPages());
		model.addAttribute("totalItems", page.getTotalElements());
		
		model.addAttribute("sortField", sortField);
		model.addAttribute("sortDir", sortDir);
		model.addAttribute("reverseSortDir", sortDir.equals("asc") ? "desc" : "asc");
		
		model.addAttribute("listCourse", listCourses);
		return "courses";

	}
}
