package com.concordia.project.controller;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * CEJV 559 - Group 2 Project
 * Error Controller - Controller to display application 400, 404, 500 error pages
 * 
 * @author Marchiano
 */
@Controller
public class SchoolErrorController implements ErrorController {
	
	/**
	 * Method from ErrorController interface
	 */
	@Override
	public String getErrorPath() {
		return null;
	}
	
	/**
	 * Display applicable error page based on error status code
	 * @param request
	 * @return corresponding error page
	 */
	@RequestMapping("/error")
	public String handleError(HttpServletRequest request) {
		Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
		
		if (status != null) {
			int statusCode = Integer.valueOf(status.toString());
			
			if (statusCode == HttpStatus.NOT_FOUND.value()) {
				return "layout/errors/error404";
			}		
			else if (statusCode == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
				return "layout/errors/error500";
			}
		}
		return "layout/errors/error400";
	}


}
