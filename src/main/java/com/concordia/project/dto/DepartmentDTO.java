package com.concordia.project.dto;



import java.io.Serializable;




/**
 * CEJV 559 - Group2 Project
 * DepartmentDTO is a model for the department table
 * Model has DepartmentDTO class with fields, constructors, getters, setters, toString
 */
/**
 * @author Elliott
 */


public class DepartmentDTO implements Serializable {
	
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	

	private int departmentId;
	private String departmentCode;
	private String description;
	private boolean disabled;
	
	
	/**
	 * Empty constructor
	 */
	public DepartmentDTO() {
		//do nothing
	}
	
	/**
	 * @param departmentId
	 * @param departmentCode
	 * @param description
	 * @param disabled
	 */
	public DepartmentDTO(int departmentId, String departmentCode, String description, boolean disabled) {
		super();
		this.departmentId = departmentId;
		this.departmentCode = generateCode();
		this.description = description;
		this.disabled = disabled;
	}

	// Getters, setters
		/**
		 * @return the departmentId
		 */
		public int getDepartmentId() {
			return departmentId;
		}

		/**
		 * @param departmentId the departmentId to set
		 */
		public void setDepartmentId(int departmentId) {
			this.departmentId = departmentId;
		}

		/**
		 * @return the departmentCode
		 */
		public String getDepartmentCode() {
			return departmentCode;
		}

		/**
		 * @param departmentCode the departmentCode to set
		 */
		public void setDepartmentCode(String departmentCode) {
			this.departmentCode = departmentCode;
		}

		/**
		 * @return the description
		 */
		public String getDescription() {
			return description;
		}

		/**
		 * @param description the description to set
		 */
		public void setDescription(String description) {
			this.description = description;
		}

		/**
		 * @return the disabled
		 */
		public boolean isDisabled() {
			return disabled;
		}

		/**
		 * @param disabled the disabled to set
		 */
		public void setDisabled(boolean disabled) {
			this.disabled = disabled;
		}

		/**
		 * @return the auto generated registrationCode
		 */
		private String generateCode() {
			return (this.description.toLowerCase());
		}

		// hash code and equals methods
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((description == null) ? 0 : description.hashCode());
			result = prime * result + (disabled ? 1231 : 1237);
			result = prime * result + ((departmentCode == null) ? 0 : departmentCode.hashCode());
			result = prime * result + (int) (departmentId ^ (departmentId >>> 32));
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			DepartmentDTO other = (DepartmentDTO) obj;
			if (description == null) {
				if (other.description != null)
					return false;
			} else if (!description.equals(other.description))
				return false;
			if (disabled != other.disabled)
				return false;
			if (departmentCode == null) {
				if (other.departmentCode != null)
					return false;
			} else if (!departmentCode.equals(other.departmentCode))
				return false;
			if (departmentId != other.departmentId)
				return false;
			return true;
		}

		/**
		 * toString method for DepartmentDTO class
		 * 
		 * @return All of the attributes of a DepartmentDTO object in one String
		 */
		@Override
		public String toString() {
			return "DepartmentDTO [departmentId=" + departmentId + ", departmentCode=" + departmentCode + ", description=" + description + ", disabled="
					+ disabled + "]";
		}
		

	
	
}
