package com.concordia.project.dto;

import java.io.Serializable;

/**
 * CEJV 559 - Group2 Project
 * RegistrationDTO is a model for the registration table
 * Model has RegistrationDTO class with fields, constructors, getters, setters, hashCode, equals and toString
 */
/**
 * @author Tetiana
 */
public class RegistrationDTO implements Serializable{

	/**
	 * serialVersion
	 */
	private static final Long serialVersionUID = 1L;
	
	private int registrationId;
	private String registrationCode;
	
	/**
	 * Empty constructor
	 */
	public RegistrationDTO() {
		// do nothing
	}

	/**
	 * @param registrationId
	 * @param registrationCode
	 */
	public RegistrationDTO(int registrationId, String registrationCode) {
		super();
		this.registrationId = registrationId;
		this.registrationCode = generateCode();
	}

	// Getters, setters
	
	/**
	 * @return the registrationId
	 */
	public int getRegistrationId() {
		return registrationId;
	}

	/**
	 * @param registrationId the registrationId to set
	 */
	public void setRegistrationId(int registrationId) {
		this.registrationId = registrationId;
	}

	/**
	 * @return the registrationCode
	 */
	public String getRegistrationCode() {
		return registrationCode;
	}

	/**
	 * @param registrationCode the registrationCode to set
	 */
	public void setRegistrationCode(String registrationCode) {
		this.registrationCode = registrationCode;
	}
	
	/**
	 * @return the auto generated registrationCode
	 */
	private String generateCode() {
		return ("Registration: " + this.registrationId);
	}

	// hash code and equals methods
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((registrationCode == null) ? 0 : registrationCode.hashCode());
		result = prime * result + (int) (registrationId ^ (registrationId >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RegistrationDTO other = (RegistrationDTO) obj;
		if (registrationCode == null) {
			if (other.registrationCode != null)
				return false;
		} else if (!registrationCode.equals(other.registrationCode))
			return false;
		if (registrationId != other.registrationId)
			return false;
		return true;
	}

	/**
	 * toString method for RegistrationDTO class
	 * 
	 * @return All of the attributes of a RegistrationDTO object in one String
	 */
	@Override
	public String toString() {
		return "RegistrationDTO [registrationId=" + registrationId + ", registrationCode=" + registrationCode + "]";
	}
	
	
	

}
