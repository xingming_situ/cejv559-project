package com.concordia.project.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.concordia.project.dto.StudentImageDTO;
import com.concordia.project.exception.ServiceException;
import com.concordia.project.model.StudentImage;

/**
 * CEJV 559 - Group 2 Project
 * StudentImageService is a service layer for the studentImage_image table
 * Service has StudentImageImageDTO interface with create, retrieve, update and delete methods for StudentImage DTOs
 */
/**
 * @author Tetiana, Marchiano
 */
@Service
public interface StudentImageService {

	/**
	 * Get a studentImage by it's ID
	 * 
	 * @param studentImageId
	 * @return StudentImage DTO with specified studentImage ID
	 */
	StudentImageDTO getStudentImage(Integer studentImageId) throws ServiceException;
	
	/**
	 * Get multiple studentImages
	 * 
	 * @return Collection of StudentImages DTOs
	 */
	List<StudentImageDTO> getAllStudentImages() throws ServiceException;

	/**
	 * Create and save a new StudentImage DTO
	 * 
	 * @param studentImageDTO An object of StudentImageDTO class
	 */
	void saveStudentImage(StudentImageDTO studentImageDTO) throws ServiceException;

	/**
	 * For saving student image object to local machine
	 * @param studentImageDTO
	 * @return StudentImage object
	 */
	StudentImage saveStudentImageFile(StudentImageDTO studentImageDTO) throws ServiceException;
	
	/**
	 * Delete a StudentImage
	 * 
	 * @param studentImageId The StudentImage ID
	 */
	void deleteStudentImage(Integer studentImageId) throws ServiceException;

	/**
	 * Update a StudentImage
	 * 
	 * @param studentImageCode The studentImage code to update with
	 * @param studentImageDTO  An object of StudentImageDTO class
	 */
	void updateStudentImage(StudentImageDTO studentImageDTO) throws ServiceException;

	/**
	 * Paginate StudentImageDTO records
	 * 
	 * @param pageNo    Page Number, integer
	 * @param pageSize  Page Size, integer
	 * @param sortField Sorting table field, String
	 * @param sortDir   Sorting type/direction, String
	 */
	Page<StudentImageDTO> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection);
}
