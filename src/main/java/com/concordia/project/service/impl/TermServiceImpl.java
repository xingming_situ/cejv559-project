package com.concordia.project.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.concordia.project.dto.TermDTO;
import com.concordia.project.exception.ServiceException;
import com.concordia.project.model.Term;
import com.concordia.project.repository.TermRepository;
import com.concordia.project.service.TermService;
import com.github.dozermapper.core.DozerBeanMapperBuilder;

/**
 * CEJV 559 - Group 2 Project Term Service Layer Implementation Implementation
 * with CRUD and pagination
 *
 * @author Tetiana, Elliott
 *
 */
@Service
public class TermServiceImpl implements TermService {

	@Autowired
	private TermRepository termRepository;

	// retrieve
	public TermDTO getTerm(Integer termId) throws ServiceException {
		Optional<Term> optional = termRepository.findById(termId);
		Term term = null;
		if (optional.isPresent()) {
			term = optional.get();
		} else {
			throw new ServiceException("Term not found for id : " + termId);
		}

		TermDTO termDTO = new TermDTO();
		BeanUtils.copyProperties(term, termDTO);
		return termDTO;
	}

	public List<TermDTO> getAllTerms() {
		List<Term> terms = this.termRepository.findAll();

		List<TermDTO> termDTOs = new ArrayList<TermDTO>(terms.size());
		for (Term term : terms) {
			TermDTO termDTO = new TermDTO();
			BeanUtils.copyProperties(term, termDTO);
			termDTOs.add(termDTO);
		}

		return termDTOs;
	}

	// create
	@Transactional
	public void saveTerm(TermDTO termDTO) throws ServiceException {
		if (termDTO == null) {
			throw new ServiceException("Term not saved");
		}
		Term term = new Term();
		BeanUtils.copyProperties(termDTO, term);
		termRepository.save(term);
	}

	// delete
	@Transactional
	public void deleteTerm(Integer termId) throws ServiceException {
		Optional<Term> termRetrieved = termRepository.findById(termId);

		if (termRetrieved.isPresent()) {
			termRepository.deleteById(termId);
		}

		throw new ServiceException("Cannot Delete: Term not found");
	}

	// update
	@Transactional
	public void updateTerm(TermDTO termDTO) throws ServiceException {
		Optional<Term> optional = termRepository.findById(termDTO.getTermId());
		Term term = null;
		if (optional.isPresent()) {
			term = optional.get();
		} else {
			throw new ServiceException("Term not found for id : " + termDTO.getTermId());
		}
		BeanUtils.copyProperties(termDTO, term);
		termRepository.save(term);

	}

	public Page<TermDTO> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection) {
		Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortField).ascending()
				: Sort.by(sortField).descending();

		Pageable pageable = PageRequest.of(pageNo - 1, pageSize, sort);

		Page<TermDTO> termDTOs = this.termRepository.findAll(pageable)
				.map((term -> DozerBeanMapperBuilder.buildDefault().map(term, TermDTO.class)));

		return termDTOs;
	}

}
