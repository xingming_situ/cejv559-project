package com.concordia.project.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.concordia.project.dto.CourseDTO;
import com.concordia.project.exception.ServiceException;
import com.concordia.project.model.Course;
import com.concordia.project.repository.CourseRepository;
import com.concordia.project.service.CourseService;
import com.github.dozermapper.core.DozerBeanMapperBuilder;

/**
 * CEJV 559 - Group 2 Project
 * Course Service Layer Implementation
 * Implementation with CRUD and pagination
 *
 * @author Elliott
 *
 */
@Service
public class CourseServiceImpl implements CourseService {


	@Autowired
	private CourseRepository courseRepository;

	// retrieve
	public CourseDTO getCourse(Integer courseId) throws ServiceException {
		Optional<Course> optional = courseRepository.findById(courseId);
		Course course = null;
		if (optional.isPresent()) {
			course = optional.get();
		} else {
			throw new ServiceException("Course not found for id : " + courseId);
		}

		CourseDTO courseDTO = new CourseDTO();
		BeanUtils.copyProperties(course, courseDTO);
		return courseDTO;
	}

	public List<CourseDTO> getAllCourses() {
		List<Course> courses = this.courseRepository.findAll();
		
		List<CourseDTO> courseDTOs = new ArrayList<CourseDTO>(courses.size());
        for (Course course : courses) {
        	CourseDTO courseDTO = new CourseDTO();
        	BeanUtils.copyProperties(course, courseDTO);
    		courseDTOs.add(courseDTO);
        }
        
        return courseDTOs;
	}

	// create
	@Transactional
	public void saveCourse(CourseDTO courseDTO) throws ServiceException {
		if (courseDTO == null) {
			throw new ServiceException("Course not saved");
		}
		Course course = new Course();
		BeanUtils.copyProperties(courseDTO, course);
		courseRepository.save(course);
	}

	// delete
	@Transactional
	public void deleteCourse(Integer courseId) throws ServiceException {
		Optional<Course> courseRetrieved = courseRepository.findById(courseId);

		if (courseRetrieved.isPresent()) {
			courseRepository.deleteById(courseId);
		}

		throw new ServiceException("Cannot Delete: Course not found");
	}

	// update
	@Transactional
	public void updateCourse(CourseDTO courseDTO) throws ServiceException {
		Optional<Course> optional = courseRepository.findById(courseDTO.getCourseId());
		Course course = null;
		if (optional.isPresent()) {
			course = optional.get();
		} else {
			throw new ServiceException("Course not found for id : " + courseDTO.getCourseId());
		}
		BeanUtils.copyProperties(courseDTO, course);
		courseRepository.save(course);

	}

	public Page<CourseDTO> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection) {
		Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortField).ascending()
				: Sort.by(sortField).descending();

		Pageable pageable = PageRequest.of(pageNo - 1, pageSize, sort);

		Page<CourseDTO> courseDTOs = this.courseRepository.findAll(pageable)
				.map((course -> DozerBeanMapperBuilder.buildDefault().map(course, CourseDTO.class)));

		return courseDTOs;
	}
}
