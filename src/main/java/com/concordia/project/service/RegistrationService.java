package com.concordia.project.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.concordia.project.dto.CourseDTO;
import com.concordia.project.dto.RegistrationDTO;
import com.concordia.project.dto.StudentDTO;
import com.concordia.project.dto.TermDTO;
import com.concordia.project.exception.ServiceException;


/**
 * CEJV 559 - Group 2 Project
 * RegistrationService is a service layer for the registration table
 * Service has RegistrationDTO interface with create, retrieve, update and delete methods for Registration DTOs
 */
/**
 * @author Tetiana
 */
@Service
public interface RegistrationService {
	
	/**
	 * Get a registration by it's ID
	 * 
	 * @param registrationId
	 * @return Registration DTO with specified registration ID
	 */
	RegistrationDTO getRegistration(Integer registrationId) throws ServiceException;
	
	/**
	 * Get multiple registrations
	 * 
	 * @return Collection of Registrations DTOs
	 */
	List<RegistrationDTO> getAllRegistrations() throws ServiceException;

	/**
	 * Create and save a new Registration DTO
	 * 
	 * @param registrationDTO An object of RegistrationDTO class
	 */
	void saveRegistration(RegistrationDTO registrationDTO, StudentDTO srudentDTO, CourseDTO courseDTO, TermDTO termDTO) throws ServiceException;

	/**
	 * Delete a Registration
	 * 
	 * @param registrationId The Registration ID
	 */
	void deleteRegistration(Integer registrationId) throws ServiceException;

	/**
	 * Update a Registration
	 * 
	 * @param registrationCode The registration code to update with
	 * @param registrationDTO  An object of RegistrationDTO class
	 */
	void updateRegistration(RegistrationDTO registrationDTO) throws ServiceException;

	/**
	 * Paginate RegistrationDTO records
	 * 
	 * @param pageNo    Page Number, integer
	 * @param pageSize  Page Size, integer
	 * @param sortField Sorting table field, String
	 * @param sortDir   Sorting type/direction, String
	 */
	Page<RegistrationDTO> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection);

	/**
	 * Show StudentDTO assigned to the registration DTO
	 * 
	 * @param registrationId The Registration ID
	 */
	StudentDTO getRegistrationStudent(Integer registrationId) throws ServiceException;
	
	/**
	 * Show CourseDTO assigned to the registration DTO
	 * 
	 * @param registrationId The Registration ID
	 */
	CourseDTO getRegistrationCourse(Integer registrationId) throws ServiceException;
	
	/**
	 * Show TermDTO assigned to the registration DTO
	 * 
	 * @param registrationId The Registration ID
	 */
	TermDTO getRegistrationTerm(Integer registrationId) throws ServiceException;
}
