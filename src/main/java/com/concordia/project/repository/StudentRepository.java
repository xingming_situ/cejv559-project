package com.concordia.project.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.concordia.project.model.Student;

/**
 * CEJV 559 - Group 2 Project
 * Repository for student table
 * Course Repository CRUD is responsible to Create, Read, Update, Delete, count, list, pagination, sorting
 */
/**
 * @author Tetiana
 */
@Repository
public interface StudentRepository extends org.springframework.data.jpa.repository.JpaRepository<Student, Integer> {

	/**
	 * Find student by specified code
	 * 
	 * @param studentCode
	 * @return Student object with specified code
	 */
	Student findByStudentCode(String studentCode);

	/**
	 * Find student by given email
	 * 
	 * @param studentEmail
	 * @return Student object with specified email
	 */
	Student findByStudentEmail(String studentEmail);

	/**
	 * Find Student by specified first and last names
	 * 
	 * @param firstName first name of the student
	 * @param lastName last name of the student
	 * @return Student object with specified first and last names
	 */
	@Query("SELECT u FROM Student u WHERE u.firstName = ?1 and u.lastName = ?2")
	Student findStudentByFirstNameAndLastName(String firstName, String lastName);

}
