package com.concordia.project.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.concordia.project.model.Teacher;

/**
 * CEJV 559 - Group 2 Project
 * Repository for teacher table
 * Teacher Repository CRUD is responsible to Create, Read, Update, Delete, count, list, pagination, sorting
 */
/**
 * @author Marchiano Oh
 */

@Repository
public interface TeacherRepository extends org.springframework.data.jpa.repository.JpaRepository<Teacher, Integer> {

	/**
	 * Find Teacher by specified teacher code
	 * 
	 * @param teacherCode
	 * @return Teacher object with specified teacher code
	 */
	Teacher findByTeacherCode(String teacherCode);

	/**
	 * Find Teacher by specified last name
	 * 
	 * @param lastName the last name of the teacher
	 * @return Teacher object with specified last name
	 */
	Teacher findByLastName(String lastName);

	/**
	 * Find Teacher by specified first and last name
	 * 
	 * @param firstName the first name of the teacher
	 * @param lastName  the last name of the teacher
	 * @return Teacher object with the specified first and last name
	 */
	@Query("SELECT t FROM Teacher t WHERE t.firstName = ?1 and t.lastName = ?2")
	Teacher findTeacherByFirstNameAndLastName(String firstName, String lastName);
}