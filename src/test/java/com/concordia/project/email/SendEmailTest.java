package com.concordia.project.email;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.concordia.project.email.service.EmailService;
import com.concordia.project.exception.EmailException;
/**
 * CEJV 559 - Group 2 Project
 * Email sending tests
 *
 * @author Tetiana
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class SendEmailTest {

    @Autowired
    private EmailService emailService;

    @Test
    public void testEmail() throws EmailException {
        try {
			emailService.sendEmail("xingmingsitu@yahoo.com", "t.savaryn@gmail.com", "Team2", "Test mail");
		} catch (Exception e) {
			throw new EmailException (e.getMessage());
		}
    }
}