package com.concordia.project.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.concordia.project.model.Course;
import com.concordia.project.model.Registration;
import com.concordia.project.model.Student;
import com.concordia.project.model.Term;

/**
 * CEJV 559 - Group 2 Project Registration repository units tests Implementation
 * with CRUD and pagination
 *
 * @author Tetiana
 *
 */
@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.ANY)
public class RegistrationRepoTest {

	@Autowired
	private RegistrationRepository registrationRepository;
	
	@Autowired
	private CourseRepository courseRepository;
	
	@Autowired
	private TermRepository termRepository;

	@Autowired
	private StudentRepository studentRepository;
	
	@Test
	public void testSaveRegistration() {
		Registration registration = new Registration("test");
		registrationRepository.save(registration);
		Registration registration1 = registrationRepository.findByRegistrationCode("test");
		assertNotNull(registration);
		assertThat(registration1.getRegistrationId()).isGreaterThan(0);
	}

	@Test
	public void testGetRegistration() {
		Registration registration = new Registration("test");
		registrationRepository.save(registration);
		Registration registration1 = registrationRepository.findByRegistrationCode("test");
		assertNotNull(registration);
		assertEquals(registration.getRegistrationCode(), registration1.getRegistrationCode());
	}

	@Test
	public void testDeleteRegistration() {
		Registration registration = new Registration("test");
		registrationRepository.save(registration);
		registrationRepository.delete(registration);
	}

	@Test
	public void findAllRegistrations() {
		int totalRecords = 5;
		for (int record = 0; record < totalRecords; record++) {
			Registration registration = new Registration("test");
			registrationRepository.save(registration);
		}
		assertEquals(registrationRepository.count(), totalRecords);
		assertNotNull(registrationRepository.findAll());
	}

	@Test
	public void deleteByRegistrationIdTest() {
		Registration registration = new Registration("test");
		Registration registration1 = registrationRepository.save(registration);
		registrationRepository.deleteById(registration1.getRegistrationId());
	}

	@Test
	public void testUpdateRegistration() {
		Registration registration = new Registration("test");
		registrationRepository.save(registration);
		assertNotNull(registration);
		Course course = new Course("Hisory", 50.00);
		Student student = new Student("John", "Doe", "johndoe@mail.com");
		Term term = new Term("Spring 2021");
		registration.setCourse(course);
		registration.setStudent(student);
		registration.setTerm(term);
		assertEquals(course, registration.getCourse());
		assertEquals(student, registration.getStudent());
		assertEquals(term, registration.getTerm());
	}

	@Test
	public void testPaginationRegistrations() {
		int totalRegistrations = 100;
		for (int registrationNumber = 0; registrationNumber < totalRegistrations; registrationNumber++) {
			Registration registration = new Registration("test");
			registrationRepository.save(registration);
		}

		assertEquals(totalRegistrations, registrationRepository.count());

		Pageable paging = PageRequest.of(1, 10, Sort.unsorted());
		Page<Registration> registrationPageResult = registrationRepository.findAll(paging);

		if (registrationPageResult.hasContent()) {
			assertEquals(registrationPageResult.getContent().size(), 10);
		}
	}

	@Test
	public void findByRegistrationCodeTest() {
		Registration registration = new Registration("test");
		registrationRepository.save(registration);
		Registration registration1 = registrationRepository.findByRegistrationCode(registration.getRegistrationCode());
		assertEquals(registration.getRegistrationCode(), registration1.getRegistrationCode());
	}

	// JUnit to add when course model is added
	@Test
	public void testSaveCourse() {
		Course course = new Course("Hisory", 50.00);
		courseRepository.save(course);
		Registration registration = new Registration("test");
		registration.setCourse(course);
		registrationRepository.save(registration);
		Registration registration1 = registrationRepository.findByRegistrationCode(registration.getRegistrationCode());
		assertNotNull(registration);
		assertSame(registration.getCourse(), registration1.getCourse());
	}

	// JUnit to add when term model is added
	@Test
	public void testSaveTerm() {
		Term term = new Term("Spring 2021");
		termRepository.save(term);
		Registration registration = new Registration("test");
		registration.setTerm(term);
		registrationRepository.save(registration);
		Registration registration1 = registrationRepository.findByRegistrationCode(registration.getRegistrationCode());
		assertNotNull(registration);
		assertSame(registration.getTerm(), registration1.getTerm());
	}
	
	// JUnit to add when student model is added
		@Test
		public void testSaveStudent() {
			Student student = new Student("John", "Doe", "johndoe@mail.com");
			studentRepository.save(student);
			Registration registration = new Registration("test");
			registration.setStudent(student);
			registrationRepository.save(registration);
			Registration registration1 = registrationRepository.findByRegistrationCode(registration.getRegistrationCode());
			assertNotNull(registration);
			assertSame(registration.getStudent(), registration1.getStudent());
		}
}
