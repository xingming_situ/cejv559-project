package com.concordia.project.repository;
/**
 * CEJV 559 - Group 2 Project
 * Department Service Layer Implementation
 * Implementation with CRUD and pagination
 *
 * @author Elliott
 *
 */
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;

import static org.assertj.core.api.Assertions.assertThat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.test.context.junit4.SpringRunner;

import com.concordia.project.model.Department;


import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;


@DataJpaTest
@RunWith(SpringRunner.class)
class DepartmentRepoTests {
	
	@Autowired
	private DepartmentRepository departmentRepository;

	@Test
	public void testSaveDepartment() {
		Department department = new Department("Science");
		Department department2 = departmentRepository.save(department);
		
		assertThat(department2.getDepartmentId()).isGreaterThan(0);
	}
	
	@Test
	public void testDeleteDepartment() {
		Department department = new Department("Science");
		departmentRepository.save(department);
		departmentRepository.delete(department);
	}
	
	@Test
	public void testSearchDepartment() {
		
		Department department = new Department("Science");
		departmentRepository.save(department);
		
		Department department2 = departmentRepository.findByDepartmentCode(department.getDepartmentCode());
		
		assertEquals(true, department2.getDepartmentCode().equalsIgnoreCase("science"));
	}
	
	@Test
	public void testUpdateDepartment() {
		
		Department department = new Department("Science");
		departmentRepository.save(department);
		
		Department department2 = departmentRepository.findByDepartmentCode("science");
		
		department2.setDepartmentCode("science2");
		departmentRepository.save(department2);
		
		Department department3 = departmentRepository.findByDepartmentCode("science2");
		assertEquals(true, department3.getDepartmentCode().equalsIgnoreCase("science2"));
		
	}
	
	@Test
	public void paginationTeachersTest() {
		int totaltests = 100;
		for (int test = 0; test < totaltests; test++) {
			Department department = new Department("name" + test);
			departmentRepository.save(department);
		}
		

		Pageable paging = PageRequest.of(1, 10, Sort.unsorted());

		Page<Department> teamPageResult = departmentRepository.findAll(paging);

		if (teamPageResult.hasContent()) {
			assertEquals(teamPageResult.getContent().size(), 10);
		}
	}

}
